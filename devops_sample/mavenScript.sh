#!/usr/bin/bash

# This script can be used to update the application version according to the commit date and the hash of the last commit. 

mvn org.codehaus.mojo:versions-maven-plugin:set -DnewVersion=$(git log -1 --format=%cd-%h --abbrev=8 --date=format:%y%m%d | tr -d '\n')

mvn clean package


# org.codehaus.mojo = groupId
# versions-maven-plugin = artifactId
# set = goal

