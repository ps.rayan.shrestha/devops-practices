#!/usr/bin/bash

set -e

githash=$(git log -1 --abbrev=8 --format=%cd-%h --date=format:%y%m%d)

if [[ ! -e target/assignment-${githash}.jar ]]
then
  mvn clean package
fi

if [[ $# -eq 0 ]]
then
  for i in $(docker ps --format '{{.Names}}')
  do
    if [[ ${i} = "h2_container" ]]
    then
      echo -e "\033[1mcontainer ${i} stop to free up port 8090\033[0m"
      docker stop $i > /dev/null
    fi
  done
  echo -e "\033[1mSPRING_PROFILES_ACTIVE=h2 will not be set.\033[0m"
  docker build -t assignment:${githash} -t assignment:mysql --build-arg GIT_VERSION=${githash} .
  docker compose -f docker-compose-mysql.yaml up -d
  exit
elif [[ $# -eq 1 ]]
then
  for i in $(docker ps --format '{{.Names}}')
  do
    if [[ ${i} = "docker_assignment" ]]
    then
      echo -e "\033[1mcontainer ${i} stop to free up port 8090\033[0m"
      docker stop $i > /dev/null
    fi
  done
  echo -e "\033[1mSPRING_PROFILES_ACTIVE=h2 will be set.\033[0m"
  docker build -t assignment:h2 --build-arg GIT_VERSION=${githash} --build-arg DATABASE_PROFILE=h2 .
  docker compose -f docker-compose-h2.yaml up -d
else
  echo "Skipped building because more than one arguments were passed"
fi

