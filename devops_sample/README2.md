# 1

We can also use the 
```mavenScript.sh``` file to set the new version of the application as well as build the package.

# 2

If we don't want to use the script then we can directly run the following command:

```bash
mvn clean package
```

# 3
If we use the second option then the ```pl.project13.maven``` plugin will work and do the task.
